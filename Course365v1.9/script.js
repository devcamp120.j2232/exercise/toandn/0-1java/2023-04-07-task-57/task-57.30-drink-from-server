"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// các combo Pizza
const gCOMBO_SMALL = "Small";
const gCOMBO_MEDIUM = "Medium";
const gCOMBO_LARGE = "Large";

// các loại pizza
const gPIZZA_TYPE_HAWAI = "Hawai";
const gPIZZA_TYPE_HAI_SAN = "HaiSan";
const gPIZZA_TYPE_BACON = "Bacon";

const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
const gCONTENT_TYPE = "application/json;charset=UTF-8";

var vComboMenu = {};

var vOrderObj = {};

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
  //gán sự kiện nút nhận small
  $("#btn-small").on("click", function () {
    onBtnSmallSizeClick();
  });
  $("#btn-medium").on("click", function () {
    onBtnMediumSizeClick();
  });
  $("#btn-large").on("click", function () {
    onBtnLargeSizeClick();
  });

  $("#btn-ocean").on("click", function () {
    onBtnHaiSanClick();
  });
  $("#btn-hawai").on("click", function () {
    onBtnHawaiClick();
  });
  $("#btn-bacon").on("click", function () {
    onBtnBaconClick();
  });
  //tạo sự kiện cho nút nhắn gửi đơn
  $("#btn-makeOder").on("click", function () {
    onBtnKiemTraDonClick();
  });
  //tạo sự kiện cho nút nhấn tạo đơn
  $("#btn-modal-create").on("click", function () {
    onBtnCreateOrderClick();
  });
  //tạo sự kiện cho nút nhấn hủy quay lại trang
  $("#btn-modal-cancel").on("click", function () {});
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm được gọi khi click nút kiểm tra đơn
function onBtnKiemTraDonClick() {
  console.log("%c Kiểm tra đơn", "color: orange;");
  // Bước 1: Đọc
  vOrderObj = getOrder();
  //Bước 2: kiểm tra
  var vKetQuaKiemTra = checkValidatedForm(vOrderObj);
  if (vKetQuaKiemTra) {
    //Bước 3: gọi và xử lý API để lấy voucher
    findVoucherByAPI(vOrderObj);
    //Bước 4: Hiển thị
    $("#info-order-modal").modal("show");
    getOrderDataToModal(vOrderObj);
  }
}

function onBtnSmallSizeClick() {
  "use strict";
  // gọi hàm đổi màu nút
  // chỉnh combo, combo được chọn là Small, đổi nút, và đánh dấu
  // data-is-selected-menu của nút Basic là Y, các nút khác là N
  changeComboButtonColor(gCOMBO_SMALL);
  //tạo một đối tượng menu, được tham số hóa
  var vSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
  // gọi method hiển thị thông tin
  vSelectedMenuStructure.displayInConsoleLog();
}
// hàm được gọi khi bấm nút chọn kích cỡ M
function onBtnMediumSizeClick() {
  "use strict";
  // gọi hàm đổi màu nút
  // chỉnh combo, combo được chọn là Medium, đổi nút, và đánh dấu
  // data-is-selected-menu của nút Basic là Y, các nút khác là N
  changeComboButtonColor(gCOMBO_MEDIUM);
  //tạo một đối tượng menu, được tham số hóa
  var vSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
  // gọi method hiển thị thông tin
  vSelectedMenuStructure.displayInConsoleLog();
}
// hàm được gọi khi bấm nút chọn kích cỡ L
function onBtnLargeSizeClick() {
  "use strict";
  // gọi hàm đổi màu nút
  // chỉnh combo, combo được chọn là Large, đổi nút, và đánh dấu
  // data-is-selected-menu của nút Basic là Y, các nút khác là N
  changeComboButtonColor(gCOMBO_LARGE);
  //tạo một đối tượng menu, được tham số hóa
  var vSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
  // gọi method hiển thị thông tin
  vSelectedMenuStructure.displayInConsoleLog();
}

// hàm đổi mầu nút khi chọn combo
function changeComboButtonColor(paramPlan) {
  var vBtnBasic = $("#btn-small"); // truy vấn nút chọn kích cỡ small
  var vBtnMedium = $("#btn-medium"); //truy vấn nút chọn kích cỡ medium
  var vBtnLarge = $("#btn-large"); //truy vấn nút chọn kích cỡ large

  if (paramPlan === gCOMBO_SMALL) {
    //nếu chọn menu Small thì thay màu nút chọn kích cỡ small bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    // đổi giá trị data-is-selected-menu
    vBtnBasic
      .prop("class", "btn btn-success col-sm-12")
      .attr("data-is-selected-menu", "Y");
    vBtnMedium
      .prop("class", "btn btn-warning col-sm-12")
      .attr("data-is-selected-menu", "N");
    vBtnLarge
      .prop("class", "btn btn-warning col-sm-12")
      .attr("data-is-selected-menu", "N");
  } else if (paramPlan === gCOMBO_MEDIUM) {
    //nếu chọn menu Medium thì thay màu nút chọn kích cỡ Medium bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    // đổi giá trị data-is-selected-menu
    vBtnBasic
      .prop("class", "btn btn-warning col-sm-12")
      .attr("data-is-selected-menu", "N");
    vBtnMedium
      .prop("class", "btn btn-success col-sm-12")
      .attr("data-is-selected-menu", "Y");
    vBtnLarge
      .prop("class", "btn btn-warning col-sm-12")
      .attr("data-is-selected-menu", "N");
  } else if (paramPlan === gCOMBO_LARGE) {
    //nếu chọn menu Large thì thay màu nút chọn kích cỡ Large bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    // đổi giá trị data-is-selected-menu
    vBtnBasic
      .prop("class", "btn btn-warning col-sm-12")
      .attr("data-is-selected-menu", "N");
    vBtnMedium
      .prop("class", "btn btn-warning col-sm-12")
      .attr("data-is-selected-menu", "N");
    vBtnLarge
      .prop("class", "btn btn-success col-sm-12")
      .attr("data-is-selected-menu", "Y");
  }
}

//function get combo (lấy menu được chọn)
//function trả lại một đối tượng combo (menu được chọn) được tham số hóa
function getComboSelected(
  paramMenuName,
  paramDuongKinhCM,
  paramSuongNuong,
  paramSaladGr,
  paramDrink,
  paramPriceVND
) {
  var vSelectedMenu = {
    menuName: paramMenuName, // S, M, L
    duongKinhCM: paramDuongKinhCM,
    suongNuong: paramSuongNuong,
    saladGr: paramSaladGr,
    drink: paramDrink,
    priceVND: paramPriceVND,
    displayInConsoleLog() {
      console.log("%cPLAN MENU COMBO - .........", "color:blue");
      console.log(this.menuName); //this = "đối tượng này"
      console.log("duongKinhCM: " + this.duongKinhCM);
      console.log("suongNuong: " + this.suongNuong);
      console.log("saladGr: " + this.saladGr);
      console.log("drink:" + this.drink);
      console.log("priceVND: " + this.priceVND);
    },
  };
  return vSelectedMenu;
}

// hàm được gọi khi bấm nút chọn loại pizza gà
function onBtnHawaiClick() {
  "use strict";
  // gọi hàm đổi màu nút
  // chỉnh loại pizza, loại pizza được chọn là GÀ , đổi nút, và đánh dấu
  // data-is-selected-pizza của nút gà là Y, các nút khác là N
  changeTypeButtonColor(gPIZZA_TYPE_HAWAI);
  // ghi loại pizza đã chọn ra console
  console.log("Loại pizza đã chọn: " + gPIZZA_TYPE_HAWAI);
}
// hàm được gọi khi bấm nút chọn loại pizza Hải sản
function onBtnHaiSanClick() {
  "use strict";
  // gọi hàm đổi màu nút
  // chỉnh loại pizza, loại pizza được chọn là Hải sản, đổi nút, và đánh dấu
  // data-is-selected-pizza của nút Hải sản là Y, các nút khác là N
  changeTypeButtonColor(gPIZZA_TYPE_HAI_SAN);
  // ghi loại pizza đã chọn ra console
  console.log("Loại pizza đã chọn: " + gPIZZA_TYPE_HAI_SAN);
}
// hàm được gọi khi bấm nút chọn loại pizza Hun khói
function onBtnBaconClick() {
  "use strict";
  // gọi hàm đổi màu nút
  // chỉnh loại pizza, loại pizza được chọn là Bacon, đổi nút, và đánh dấu
  // data-is-selected-pizza của nút Bacon là Y, các nút khác là N
  changeTypeButtonColor(gPIZZA_TYPE_BACON);
  // ghi loại pizza đã chọn ra console
  console.log("Loại pizza đã chọn: " + gPIZZA_TYPE_BACON);
}

// hàm đổi mầu nút khi chọn loại pizza
function changeTypeButtonColor(paramType) {
  var vBtnHawai = $("#btn-hawai"); // truy vấn nút chọn loại pizza gà
  var vBtnHaiSan = $("#btn-ocean"); //truy vấn nút chọn loại pizza hải sản
  var vBtnBacon = $("#btn-bacon"); //truy vấn nút chọn loại pizza bacon

  if (paramType === gPIZZA_TYPE_HAWAI) {
    //nếu chọn loại pizza Gà thì thay màu nút chọn loại pizza gà bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    // đổi giá trị data-is-selected-pizza
    vBtnHawai
      .prop("class", "btn btn-success w-100")
      .attr("data-is-selected-pizza", "Y");
    vBtnHaiSan
      .prop("class", "btn btn-warning w-100")
      .attr("data-is-selected-pizza", "N");
    vBtnBacon
      .prop("class", "btn btn-warning w-100")
      .attr("data-is-selected-pizza", "N");
  } else if (paramType === gPIZZA_TYPE_HAI_SAN) {
    //nếu chọn loại pizza Hải sản thì thay màu nút chọn loại pizza Hải sản bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    // đổi giá trị data-is-selected-pizza
    vBtnHawai
      .prop("class", "btn btn-warning w-100")
      .attr("data-is-selected-pizza", "N");
    vBtnHaiSan
      .prop("class", "btn btn-success w-100")
      .attr("data-is-selected-pizza", "Y");
    vBtnBacon
      .prop("class", "btn btn-warning w-100")
      .attr("data-is-selected-pizza", "N");
  } else if (paramType === gPIZZA_TYPE_BACON) {
    //nếu chọn loại pizza Bacon thì thay màu nút chọn loại pizza Bacon bằng màu cam (btn-warning), hai nút kia xanh (btn-success)
    // đổi giá trị data-is-selected-pizza
    vBtnHawai
      .prop("class", "btn btn-warning w-100")
      .attr("data-is-selected-pizza", "N");
    vBtnHaiSan
      .prop("class", "btn btn-warning w-100")
      .attr("data-is-selected-pizza", "N");
    vBtnBacon
      .prop("class", "btn btn-success w-100")
      .attr("data-is-selected-pizza", "Y");
  }
}

// hàm được gọi khi loading nhằm lấy list select drink
function onPageLoading() {
  getDataComboMenu();
  getDrinkListAjaxClick();
}

//hàm gọi api combomenu
function getDataComboMenu() {
  "use strict";
  $.ajax({
    url: "http://localhost:8080/api/combo-menu",
    dataType: "json",
    type: "GET",
    success: function (res) {
      vComboMenu = res;
      console.log(vComboMenu);
      displayComboMenu(vComboMenu);
    },
  });
}

function displayComboMenu(paramComboMenu) {
  var vMenu = $("#combo-menu");
  var vSize = vMenu.find(".size");
  var vDuongKinh = vMenu.find(".duongkinh");
  var vSuonNuong = vMenu.find(".suonnuong");
  var vSalad = vMenu.find(".salad");
  var vNuocNgot = vMenu.find(".nuocngot");
  var vDonGia = vMenu.find(".vnd");

  for (var i = 0; i < paramComboMenu.length; i++) {
    vSize.eq(i).text(paramComboMenu[i].size);
    vDuongKinh.eq(i).text(paramComboMenu[i].duongkinh);
    vSuonNuong.eq(i).text(paramComboMenu[i].suonnuong);
    vSalad.eq(i).text(paramComboMenu[i].salad);
    vNuocNgot.eq(i).text(paramComboMenu[i].nuocngot);
    vDonGia.eq(i).text(paramComboMenu[i].vnd);
  }
}

//hàm gọi api select drink
function getDrinkListAjaxClick() {
  "use strict";
  $.ajax({
    url: "http://localhost:8080/api/drinks",
    dataType: "json",
    type: "GET",
    success: function (res) {
      handleDrinkList(res);
    },
    error: function (ajaxContent) {
      alert(ajaxContent.responseText);
    },
  });
}

//hàm tạo option cho select đồ uống
function handleDrinkList(paramDrink) {
  "use strict";
  $.each(paramDrink, function (i, item) {
    $("#select-drink").append(
      $("<option>", {
        text: item.tenNuocUong,
        value: item.maNuocUong,
      })
    );
  });
}

// hàm thu thập(đọc) thông tin khách hàng
function getOrder() {
  var vBtnBasic = $("#btn-small"); // truy vấn nút chọn kích cỡ small
  var vMenuBasicDaChon = vBtnBasic.attr("data-is-selected-menu");
  var vBtnMedium = $("#btn-medium"); //truy vấn nút chọn kích cỡ medium
  var vMenuMediumDaChon = vBtnMedium.attr("data-is-selected-menu");
  var vBtnLarge = $("#btn-large"); //truy vấn nút chọn kích cỡ large
  var vMenuLargeDaChon = vBtnLarge.attr("data-is-selected-menu");
  var vSelectedMenuStructure = getComboSelected("", 0, 0, 0, 0, 0);
  if (vMenuBasicDaChon === "Y") {
    vSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
  } else if (vMenuMediumDaChon === "Y") {
    vSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
  } else if (vMenuLargeDaChon === "Y") {
    vSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
  }
  var vBtnHawai = $("#btn-hawai"); // truy vấn nút chọn loại pizza gà
  var vPizzaHawaiDaChon = vBtnHawai.attr("data-is-selected-pizza");
  var vBtnHaiSan = $("#btn-ocean"); //truy vấn nút chọn loại pizza hải sản
  var vPizzaHaiSanDaChon = vBtnHaiSan.attr("data-is-selected-pizza");
  var vBtnBacon = $("#btn-bacon"); //truy vấn nút chọn loại pizza bacon
  var vPizzaBaconDaChon = vBtnBacon.attr("data-is-selected-pizza");
  var vLoaiPizza = "";
  if (vPizzaHawaiDaChon === "Y") {
    vLoaiPizza = gPIZZA_TYPE_HAWAI;
  } else if (vPizzaHaiSanDaChon === "Y") {
    vLoaiPizza = gPIZZA_TYPE_HAI_SAN;
  } else if (vPizzaBaconDaChon === "Y") {
    vLoaiPizza = gPIZZA_TYPE_BACON;
  }
  var vValueInputName = $("#inp-fullname").val();
  var vValueInputEmail = $("#inp-email").val();
  var vValueInputPhone = $("#inp-dien-thoai").val();
  var vValueInputAddress = $("#inp-dia-chi").val();
  var vValueInputMessage = $("#inp-message").val();
  var vValueInputVoucherID = $("#inp-voucher-id").val();
  var vCodeDrink = $("#select-drink").val();

  var vPercent = 0;
  var vOrderInfo = {
    menuCombo: vSelectedMenuStructure,
    loaiPizza: vLoaiPizza,
    loaiNuocUong: vCodeDrink,
    hoVaTen: vValueInputName,
    email: vValueInputEmail,
    dienThoai: vValueInputPhone,
    diaChi: vValueInputAddress,
    loiNhan: vValueInputMessage,
    voucher: vValueInputVoucherID,
    phanTramGiamGia: vPercent,
    priceAnnualVND: function () {
      var vTotal = this.menuCombo.priceVND * (1 - this.phanTramGiamGia / 100);

      return vTotal;
    },
  };
  return vOrderInfo;
}

// ham kiem tra thong tin dat hang
// input: doi tuong khach hang
// output: dung/ sai (true/ false)
function checkValidatedForm(paramOrder) {
  if (paramOrder.menuCombo.menuName === "") {
    alert("Chọn combo pizza...");
    return false;
  }
  if (paramOrder.loaiPizza === "") {
    alert("Chọn loại pizza...");
    return false;
  }
  if (paramOrder.loaiNuocUong == 0) {
    alert("Chọn loại nước uống...");
    return false;
  }
  if (paramOrder.hoVaTen === "") {
    alert("Nhập họ và tên");
    return false;
  }
  if (isEmail(paramOrder.email) == false) {
    return false;
  }

  if (paramOrder.dienThoai === "") {
    alert("Nhập vào số điện thoại...");
    return false;
  }
  if (paramOrder.diaChi === "") {
    alert("Địa chỉ không để trống...");
    return false;
  }
  return true;
}

//gọi và xử lý API để lấy voucher
function findVoucherByAPI(paramOrder) {
  var vPhanTramGiamGia = 0;
  $.ajax({
    url:
      "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" +
      paramOrder.voucher,
    type: "GET",
    async: false,
    dataType: "json",
    success: function (responseObject) {
      var vVoucherObject = responseObject;
      vPhanTramGiamGia = vVoucherObject.phanTramGiamGia;
    },
    error: function (error) {
      console.assert(error.responseText);
      vPhanTramGiamGia = 0;
    },
  });
  paramOrder.phanTramGiamGia = vPhanTramGiamGia;
}

//hàm kiểm tra email
function isEmail(paramEmail) {
  if (paramEmail < 3) {
    alert("Nhập email...");
    return false;
  }
  if (paramEmail.indexOf("@") === -1) {
    alert("Email phải có ký tự @");
    return false;
  }
  if (paramEmail.startsWith("@") === true) {
    alert("Email không bắt đầu bằng @");
    return false;
  }
  if (paramEmail.endsWith("@") === true) {
    alert("Email kết thúc bằng @");
    return false;
  }
  return true;
}

//hàm thu thập dữ liệu form modal
function getOrderDataToModal(paramOrderObj) {
  var vInforDetail1 =
    "Xác nhận: " +
    paramOrderObj.hoVaTen +
    ", " +
    paramOrderObj.dienThoai +
    ", " +
    paramOrderObj.diaChi +
    "\r\n" +
    "Menu " +
    paramOrderObj.menuCombo.menuName +
    ", sườn nướng " +
    paramOrderObj.menuCombo.suongNuong +
    ", nước " +
    paramOrderObj.menuCombo.drink +
    "..." +
    "\r\n" +
    "Loại pizza: " +
    paramOrderObj.loaiPizza +
    ", Giá: " +
    paramOrderObj.menuCombo.priceVND +
    ", Mã giảm giá: " +
    paramOrderObj.voucher +
    "\r\n" +
    "Phải thanh toán: " +
    paramOrderObj.priceAnnualVND() +
    "vnd (giảm giá " +
    paramOrderObj.phanTramGiamGia +
    "%)";
  $("#input-hoTen").val(paramOrderObj.hoVaTen);
  $("#input-soDienThoai").val(paramOrderObj.dienThoai);
  $("#input-diaChi").val(paramOrderObj.diaChi);
  $("#input-loiNhan").val(paramOrderObj.loiNhan);
  $("#input-maGiamGia").val(paramOrderObj.voucher);
  $("#textarea-inforDetail").text(vInforDetail1);
}

function onBtnCancelOrderClick() {
  $("#info-order-modal").modal("hide");
}

function onBtnCreateOrderClick() {
  $("#info-order-modal").modal("hide");
  $("#order-confirm-modal").modal("show");

  callAPIGetDataOrder(vOrderObj);
}

//hàm gọi API push Order
function callAPIGetDataOrder(paramOrderObj) {
  //insert order
  $.ajax({
    url: gBASE_URL,
    type: "POST",
    contentType: gCONTENT_TYPE,
    data: JSON.stringify(paramOrderObj),
    success: function (paramRes) {
      // B4: xử lý front-end
      handleInsertCourseSuccess(paramRes);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
  });
}
// hàm xử lý hiển thị front-end khi thêm course thành công
function handleInsertCourseSuccess(paramResponseOrder) {
  $("#order-confirm-modal").modal("show");
  var vOrderCode = paramResponseOrder.orderCode;
  $("#confirm-order-code-modal").val(vOrderCode);
}
