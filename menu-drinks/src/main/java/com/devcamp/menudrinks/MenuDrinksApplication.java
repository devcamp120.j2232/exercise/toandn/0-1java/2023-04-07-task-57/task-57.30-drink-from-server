package com.devcamp.menudrinks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MenuDrinksApplication {

	public static void main(String[] args) {
		SpringApplication.run(MenuDrinksApplication.class, args);
	}

}
