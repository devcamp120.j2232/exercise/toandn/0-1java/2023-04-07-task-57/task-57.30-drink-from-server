package com.devcamp.menudrinks.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.devcamp.menudrinks.models.CDrink;

@Service
public class CDrinkService {
  Date now = new Date();
  long time = now.getTime();

  CDrink drink1 = new CDrink("TRATAC  ", "Trà tắc", 10000, null, time, time);
  CDrink drink2 = new CDrink("COCA", "Cocacola", 15000, null, time, time);
  CDrink drink3 = new CDrink("PEPSI", "Pepsi", 15000, null, time, time);
  CDrink drink4 = new CDrink("LAVIE  ", "Lavie", 5000, null, time, time);
  CDrink drink5 = new CDrink("TRASUA", "Trà sữa trân châu", 40000, null, time, time);
  CDrink drink6 = new CDrink("FANTA", "Fanta", 15000, null, time, time);

  ArrayList<CDrink> drinks = new ArrayList<>();

  public ArrayList<CDrink> getDrinks() {
    return drinks;
  }

  public ArrayList<CDrink> getListDrink() {
    ArrayList<CDrink> listdrink = new ArrayList<>();
    listdrink.add(drink1);
    listdrink.add(drink2);
    listdrink.add(drink3);
    listdrink.add(drink4);
    listdrink.add(drink5);
    listdrink.add(drink6);

    drinks.addAll(listdrink);

    return listdrink;
  }
}
