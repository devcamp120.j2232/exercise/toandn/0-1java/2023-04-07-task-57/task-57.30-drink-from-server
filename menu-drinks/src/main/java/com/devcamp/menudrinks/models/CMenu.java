package com.devcamp.menudrinks.models;

public class CMenu {
  String size;
  String duongkinh;
  int suonnuong;
  String salad;
  int nuocngot;
  String vnd;

  public CMenu(String size, String duongkinh, int suonnuong, String salad, int nuocngot, String vnd) {
    this.size = size;
    this.duongkinh = duongkinh;
    this.suonnuong = suonnuong;
    this.salad = salad;
    this.nuocngot = nuocngot;
    this.vnd = vnd;
  }

  public String getSize() {
    return size;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public String getDuongkinh() {
    return duongkinh;
  }

  public void setDuongkinh(String duongkinh) {
    this.duongkinh = duongkinh;
  }

  public int getSuonnuong() {
    return suonnuong;
  }

  public void setSuonnuong(int suonnuong) {
    this.suonnuong = suonnuong;
  }

  public String getSalad() {
    return salad;
  }

  public void setSalad(String salad) {
    this.salad = salad;
  }

  public int getNuocngot() {
    return nuocngot;
  }

  public void setNuocngot(int nuocngot) {
    this.nuocngot = nuocngot;
  }

  public String getVnd() {
    return vnd;
  }

  public void setVnd(String vnd) {
    this.vnd = vnd;
  }

}
