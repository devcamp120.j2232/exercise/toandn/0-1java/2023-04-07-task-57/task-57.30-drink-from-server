package com.devcamp.menudrinks.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.menudrinks.models.CDrink;
import com.devcamp.menudrinks.service.CDrinkService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CDrinkController {
  @Autowired
  private CDrinkService cdDrinkService;

  @GetMapping("/drinks")
  public ArrayList<CDrink> getListDrinks() {
    return cdDrinkService.getListDrink();
  }
}
